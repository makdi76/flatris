FROM node

RUN mkdir /app
#WORKDIR /app

COPY package.json /app
WORKDIR /app
RUN yarn install

COPY . /app

EXPOSE 3010

#RUN yarn test
RUN yarn build
CMD yarn start

